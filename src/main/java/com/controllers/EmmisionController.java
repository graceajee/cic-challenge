package com.controllers;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.daoimpl.EmmisionDaoImpl;


@Controller
@RequestMapping("emmision")

public class EmmisionController {
	final static Logger logger = Logger.getLogger(EmmisionController.class);

	@RequestMapping(value = "/jsondata", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getEmmisionData() {
		EmmisionDaoImpl emmisiondaoimpl = new EmmisionDaoImpl();
		return emmisiondaoimpl.callURL("https://data.sfgov.org/resource/y2ju-xyjc.json", "noparameter");

	}

	@RequestMapping(value = "/jsondata/{fuel}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getEmmisionDataSearch(@PathVariable("fuel") String fuel) {
		EmmisionDaoImpl emmisiondaoimpl = new EmmisionDaoImpl();
		return emmisiondaoimpl.callURL("https://data.sfgov.org/resource/y2ju-xyjc.json", fuel);

	}

}