package com.controllers;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.daoimpl.FilmDaoImpl;


@Controller
@RequestMapping("filmen")
public class FilmlistController {
	final static Logger logger = Logger.getLogger(FilmlistController.class);
	
	@RequestMapping(value = "/jsondata", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getFilmdata() {
		FilmDaoImpl filmdaoimpl = new FilmDaoImpl();
		return filmdaoimpl.callURL("https://data.sfgov.org/resource/wwmu-gmzc.json", "noparameter");
	}

	@RequestMapping(value = "/jsondata/{title}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getFilmdatasearch(@PathVariable("title") String title) {
		FilmDaoImpl filmdaoimpl = new FilmDaoImpl();
		return filmdaoimpl.callURL("https://data.sfgov.org/resource/wwmu-gmzc.json", title);
	}
}
