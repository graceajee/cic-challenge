package com.entities;

public class Films {
	protected String filmName;
	protected String location;

	public Films(String filmName, String location) {
		this.filmName = filmName;
		this.location = location;
	}

	public Films() {
	}

	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
