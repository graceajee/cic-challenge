package com.servicesimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.daoapi.FilmDao;
import com.entities.Films;
import com.servicesapi.FilmService;

public class FilmServiceImpl implements FilmService {
	@Autowired
	FilmDao filmDao;

	public List<Films> callURL(String filmName, String titlefilter) {
		// TODO Auto-generated method stub
		return filmDao.callURL(filmName, titlefilter);
	}
}
