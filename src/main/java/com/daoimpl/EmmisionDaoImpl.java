package com.daoimpl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

public class EmmisionDaoImpl {
	public List emmisionList;
	public List callURL(String myURL, String sourcetype) {
		emmisionList = new ArrayList();
		System.out.println("Requested URL:" + myURL);
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		try {
			URL url = new URL(myURL);
			urlConn = url.openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					int cp;
					while ((cp = bufferedReader.read()) != -1) {
						sb.append((char) cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:" + myURL, e);
		}

		try {

			JSONArray jsonArray = new JSONArray(sb.toString());
			JSONObject emmisionjsonObj = new JSONObject();

			if (jsonArray != null) {

				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					JSONObject rec = jsonArray.getJSONObject(i);

					if (rec.has("department")) {
						String department = rec.getString("department");
						String emissions_mtco2e = rec.getString("emissions_mtco2e");
						String source_type = rec.getString("source_type");
						String searchDepartment = "San Francisco";
						String fuelType="propane  B20  Gasoline CNG B5 Diesel";
						
						if (department.toLowerCase().indexOf(searchDepartment.toLowerCase()) != -1 && emissions_mtco2e !=null &&  Double.parseDouble(emissions_mtco2e)>0) {
						
							if ("noparameter".equals(sourcetype)) {
								emmisionjsonObj.put("emissions_mtco2e", emissions_mtco2e);
								emmisionjsonObj.put("department", department);
								emmisionList.add(emmisionjsonObj.toString());
							}else if ("fuel".equals(sourcetype) && fuelType.toLowerCase().indexOf(source_type.toLowerCase()) != -1) {
								emmisionjsonObj.put("emissions_mtco2e", emissions_mtco2e);
								emmisionjsonObj.put("department", department);
								emmisionjsonObj.put("source_type", source_type);
								emmisionList.add(emmisionjsonObj.toString());
							}else {
								emmisionjsonObj.put("No such Data found! ", "search only fuel types like'"+fuelType+"' ");
								emmisionList.add(emmisionjsonObj.toString());
							}
						}

					}

					
				}

			}
		} catch (JSONException e) {
			System.out.println(e);
		}
		catch (NumberFormatException  e) {
			System.out.println(e);
		}
		
		return emmisionList;
	}

}