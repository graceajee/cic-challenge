package com.daoimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import com.daoapi.FilmDao;

@Component
public class FilmDaoImpl implements FilmDao {

	public List filmList;
	public List callURL(String myURL, String titlefilter) {
		filmList = new ArrayList();	
		System.out.println("Requested URL:" + myURL);
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		try {
			URL url = new URL(myURL);
			urlConn = url.openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					int cp;
					while ((cp = bufferedReader.read()) != -1) {
						sb.append((char) cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:" + myURL, e);
		}

		try {

			JSONArray jsonArray = new JSONArray(sb.toString());
			JSONObject filmjsonObj = new JSONObject();

			if (jsonArray != null) {

				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					JSONObject rec = jsonArray.getJSONObject(i);

					if (rec.has("locations")) {
						String loc = rec.getString("locations");

						String title = rec.getString("title");
						String searchLocation = "San Francisco";

						if (loc.toLowerCase().indexOf(searchLocation.toLowerCase()) != -1) {

							if ("noparameter".equals(titlefilter)) {
								filmjsonObj.put("title", title);
								filmjsonObj.put("locations", loc);
								filmList.add(filmjsonObj.toString());
							} else if (!"noparameter".equals(titlefilter)
									&& title.toLowerCase().indexOf(titlefilter.toLowerCase()) != -1) {

								filmjsonObj.put("title", title);
								filmjsonObj.put("locations", loc);
								filmList.add(filmjsonObj.toString());
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			System.out.println(e);
		}
		return filmList;
	}
}
