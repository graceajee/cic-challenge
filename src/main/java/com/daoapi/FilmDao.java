package com.daoapi;

import java.util.List;
import com.entities.Films;

public interface FilmDao {
	public List<Films> callURL(String filmName, String titlefilter);
}
