package com.servicesapi;

import java.util.List;
import com.entities.Films;

public interface FilmService {
	public List<Films> callURL(String filmName, String titlefilter);
}
