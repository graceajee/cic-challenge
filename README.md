#Process Documents


1. ##Intended 
	The Services is intended to read the JSON data format from two different external sources
	- https://data.sfgov.org/resource/wwmu-gmzc.json
	- https://data.sfgov.org/resource/y2ju-xyjc.json 

	Retrieve and deliver the specific data objects as mentioned in the provided script from "cic challenge.eu" in the json format as required.

2. ##Technologies used
	

	* Data: JSON Format	
	* Programming Language: Java / JEE
	* Frame Work: Spring MVC
	* Web Services: Restful
	* Web Server: Tomcat 9
	* IDE: Eclipse
	* Protocol: HTTP/HTTPS
	* Project Management: Maven
	* JSP/HTML
	* Css
	* Javascript 
	
3.	## Communication Interface 
	HTTP/HTTPS protocol

4.	## Services Functionality
	* **How to deal**	
		- Project Title:IbmMVC (import as maven Project)
		- User Interface: http://localhost:8080/IbmMVC/
			- Options
				- Drehorte in San Francisco
					- To direct access to retrieve the Films data that are shot in San Francisco go to (localhost:port/IbmMVC/filmen/jsondata)
					- To direct access to retrieve the Films data filter by title go to (localhost:port/IbmMVC/filmen/jsondata/title).
				<br/><br/>
				- Co2 Emmisioen
					- To direct access to retrieve the Dipartments in San Fancisco that deleiver CO2 > 0 go to (localhost:port/IbmMVC/emmision/jsondata)
					- To direct access to retrieve the Departments that deliever only fuel Types and CO2 > 0 go to (localhost:port/IbmMVC/emmision/jsondata/fuel)
			



  
 